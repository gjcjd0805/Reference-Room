package hello.core.order;

import hello.core.AppConfig;
import hello.core.member.Grade;
import hello.core.member.Member;
import hello.core.member.MemberService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class OrderServiceTest {

    MemberService memberService;
    OrderService orderService;


    @BeforeEach
    void beforeEach(){
        AppConfig appConfig = new AppConfig();
        memberService = appConfig.memberService();
        orderService = appConfig.orderService();

    }

    @Test
    void createOrder(){
        Long memberId = 1L;
        memberService.join(new Member(memberId,"허청", Grade.VIP));

        Order oder = orderService.createOrder(memberId,"꽃",5000);

        Assertions.assertThat(oder.getDiscountPrice()).isEqualTo(1000);
    }
}
