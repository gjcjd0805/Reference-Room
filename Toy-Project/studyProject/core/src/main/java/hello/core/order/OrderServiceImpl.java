package hello.core.order;

import hello.core.discount.DiscountPolicy;
import hello.core.discount.RateDiscountPolicy;
import hello.core.member.Member;
import hello.core.member.MemberRepository;
import hello.core.member.MemoryMemberRepository;

public class OrderServiceImpl implements OrderService{

    private final MemberRepository memberRepository;
    /*private final DiscountPolicy discountPolicy = new FixDiscountPolicy();*/
    private final DiscountPolicy discountPolicy;

    public OrderServiceImpl(MemberRepository memberRepository, DiscountPolicy discountPolicy) {
        this.memberRepository = memberRepository;
        this.discountPolicy = discountPolicy;
    }

    @Override
    public Order createOrder(Long memberId, String itemName,int itemPrice) {
        Member member = memberRepository.findById(memberId);                // 회원조회
        int discountPrice = discountPolicy.discount(member,itemPrice);      // 할인정책적용
        Order order = new Order(memberId,itemName,itemPrice,discountPrice); // 주문 반환

        return order;
    }
}
