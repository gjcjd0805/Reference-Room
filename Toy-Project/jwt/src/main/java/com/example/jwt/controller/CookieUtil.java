package com.example.jwt.controller;

import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

@Service
public class CookieUtil {

    // Cookie Create
    public Cookie createCookie(String cookieName, String value){
        Cookie token = new Cookie(cookieName,value);
        token.setHttpOnly(true);
        token.setMaxAge((int)JwtUtil.TOKEN_VALIDATION_SECOND);
        token.setPath("/");
        return token;
    }

    // Cookie Get
    public Cookie getCookie(HttpServletRequest req, String cookieName){
        final Cookie[] cookies = req.getCookies();
        if(cookies==null) {
            return null;
        }
        for(Cookie cookie : cookies){

            if(cookie.getName().equals(cookieName)) {
                cookie.setHttpOnly(true);
                return cookie;
            }
        }
        return null;
    }

}
