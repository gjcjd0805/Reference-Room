package com.example.jwt.controller;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN
}
