package com.example.jwt.controller;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Date;

@Component
@Slf4j
public class JwtUtil implements InitializingBean {


    public final static long TOKEN_VALIDATION_SECOND = 1800L; // 30분

    public final static long REFRESH_TOKEN_VALIDATION_SECOND = 3600L * 24; // 1일

    public final static String accessTokenName = "accessToken";

    public final static String refreshTokenName = "accessToken";

    @Value("${spring.jwt.secret}")
    private String secretKey;

    private Key signingKey;

    // JWT SigningKey 생성
    @Override
    public void afterPropertiesSet() throws Exception {
        byte[] keyBytes = secretKey.getBytes(StandardCharsets.UTF_8);
        this.signingKey =  Keys.hmacShaKeyFor(keyBytes);
    }

    // JWT Claim 정보 가져오기
    public Claims extractAllClaims(String token) throws ExpiredJwtException {
        return Jwts.parserBuilder()
                .setSigningKey(signingKey)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    // JWT Claim의 username 가져오기
    public String getUsername(String token) {
        return extractAllClaims(token).get("memberId", String.class);
    }

    // JWT 엑세스 토큰 생성
    public String generateToken(User user) {
        return createToken(user.getId(), TOKEN_VALIDATION_SECOND);
    }

    // JWT 리플레쉬 토큰 생성
    public String generateRefreshToken(User user) {
        return createToken(user.getId(), REFRESH_TOKEN_VALIDATION_SECOND);
    }

    // JWT 토큰 생성 템플릿
    public String createToken(Long memberId, Long expireTime) {

        Claims claims = Jwts.claims();
        claims.put("memberId", memberId);

        String jwt = Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + expireTime))
                .signWith(signingKey, SignatureAlgorithm.HS512)
                .compact();

        return jwt;
    }
    // 토큰 유효성 검증
    public Boolean validateToken(String token) {

        try {
            // 토큰을 파싱해보고 발생하는 exception들을 캐치, 문제가 생기면 false, 정상이면 true를 return
            Jwts.parserBuilder().setSigningKey(signingKey).build().parseClaimsJws(token);
            return true;
        } catch (io.jsonwebtoken.security.SecurityException | MalformedJwtException e) {
            log.info("잘못된 JWT 서명입니다.");
        } catch (ExpiredJwtException e) {
            log.info("만료된 JWT 토큰입니다.");
        } catch (UnsupportedJwtException e) {
            log.info("지원되지 않는 JWT 토큰입니다.");
        } catch (IllegalArgumentException e) {
            log.info("JWT 토큰이 잘못되었습니다.");
        }
        return false;
    }


}
