package com.example.jwt;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Override // 스프링 시큐리티 적용 제외
    public void configure(WebSecurity web){
        web.ignoring().antMatchers("/error","/h2-console/**","favicon.ico");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception{
        http.authorizeRequests().antMatchers("/api/health-check").permitAll()
                .anyRequest().authenticated();
    }


}
