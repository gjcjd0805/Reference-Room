#!/bin/bash

#1. yum 업데이트
sudo yum update -y

#2. 리스트 확인
sudo yum list java*jdk-devel

#3. java 설치 
sudo yum install -y java-1.8.0-openjdk-devel.x86_64

#4. java 버전확인
java -version

#5. 자바 설치 경로확인
A=$(which javac)
B=$(readlink -f ${A})
C=${B%/bin/javac}
echo A : ${A}
echo B : ${B}
echo C : ${C}

#6. readlink를 통해 심볼릭 링크가 연결
ls -l /usr/bin/javac
readlink /usr/lib/jvm/java-1.8.0-openjdk-devel.x86_64/bin/javac

# 7. 환경변수세팅
# 환경변수는 모든 사용자와 해당 사용자에게 적용 시키는 방법
# /etc/profile (root계정 : 모든 사용자에게 적용)
# /etc/bashrc (root계정 : 모든 사용자에게 적용)
# ~/.bashrc (사용자계정 : 해당 사용자에게만 적용)
# ~/.bash_profile (사용자 계정 : 해당 사용자에게만 적용)
echo -e "export JAVA_HOME=${C} \n export PATH=${C}/bin:${PATH}" | sudo tee -a /etc/profile > /dev/null
source /etc/profile

# 8.재부팅
sudo reboot

