#!/bin/sh
# ubuntu 22.04 

#1. apt-get 업데이트
sudo apt-get update
#2. JDK 설치(JDK 8 이상의 원하는 버전을 설치한다.)
sudo apt-get install -y openjdk-8-jdk
#3. Jenkins 저장소 Key 다운로드
wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -
#4. sources.list.d 에 jenkins.list 추가
echo deb http://pkg.jenkins.io/debian-stable binary/ | sudo tee /etc/apt/sources.list.d/jenkins.list
#5. Key 등록
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys FCEF32E745F2C3D5
#6. apt-get 재 업데이트
sudo apt-get update
#7. Jenkins 설치
sudo apt-get install -y jenkins
#8. Jenkins 서버 포트 번호 변경
#sudo vi /etc/default/jenkins
#9. Jenkins 서비스 재기동 
#sudo service jenkins restart
#10. Jenkins 서비스 상태 확인
#sudo systemctl status jenkins
#11. Jenkins 초기 비밀번호 확인
echo "젠킨스 초기 비밀번호"
sudo cat /var/lib/jenkins/secrets/initialAdminPassword

