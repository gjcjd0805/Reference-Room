#!/bin/bash

# wget 설치(선택사항)
sudo yum install wget

# gradle.zip 다운로드
sudo wget https://services.gradle.org/distributions/gradle-7.0.2-bin.zip

# unzip 설치(선택사항)
sudo yum install unzip

# 압축해제
sudo mkdir -p /opt/gradle; sudo unzip -d /opt/gradle gradle-7.0.2-bin.zip
sudo ls -al /opt/gradle/gradle-7.0.2

GRADLE_PATH_A=/opt/gradle/gradle-7.0.2

echo -e "#!/bin/bash \n export GRADLE_HOME=${GRADLE_PATH_A} \n export PATH=${GRADLE_PATH_A}/bin:${PATH}" | sudo tee -a /etc/profile.d/gradle.sh > /dev/null
sudo chmod +x /etc/profile.d/gradle.sh; source /etc/profile.d/gradle.sh

gradle -v
